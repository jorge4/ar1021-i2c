/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/***************************************************
 * This is a library for the Adafruit STMPE610 Resistive
 * touch screen controller breakout
 * ----> http://www.adafruit.com/products/1571
 *
 * Check out the links above for our tutorials and wiring diagrams
 * These breakouts use SPI or I2C to communicate
 *
 * Adafruit invests time and resources providing this open source code,
 * please support Adafruit and open-source hardware by purchasing
 * products from Adafruit!
 *
 * Written by Limor Fried/Ladyada for Adafruit Industries.
 * MIT license, all text above must be included in any redistribution
 ****************************************************/

/* Adapted for native Mongoose by Pim van Pelt <pim@google.com> */

#ifndef __AR1021H_
#define __AR1021H_

#include "mgos.h" 


/******************************************************************************
 * Defines and typedefs
 *****************************************************************************/
 
#define AR1021_REG_TOUCH_THRESHOLD        (0x02)
#define AR1021_REG_SENS_FILTER            (0x03)
#define AR1021_REG_SAMPLING_FAST          (0x04)
#define AR1021_REG_SAMPLING_SLOW          (0x05)
#define AR1021_REG_ACC_FILTER_FAST        (0x06)
#define AR1021_REG_ACC_FILTER_SLOW        (0x07)
#define AR1021_REG_SPEED_THRESHOLD        (0x08)
#define AR1021_REG_SLEEP_DELAY            (0x0A)
#define AR1021_REG_PEN_UP_DELAY           (0x0B)
#define AR1021_REG_TOUCH_MODE             (0x0C)
#define AR1021_REG_TOUCH_OPTIONS          (0x0D)
#define AR1021_REG_CALIB_INSETS           (0x0E)
#define AR1021_REG_PEN_STATE_REPORT_DELAY (0x0F)
#define AR1021_REG_TOUCH_REPORT_DELAY     (0x11)
 
 
#define AR1021_CMD_GET_VERSION                 (0x10)
#define AR1021_CMD_ENABLE_TOUCH                (0x12)
#define AR1021_CMD_DISABLE_TOUCH               (0x13)
#define AR1021_CMD_CALIBRATE_MODE              (0x14)
#define AR1021_CMD_REGISTER_READ               (0x20)
#define AR1021_CMD_REGISTER_WRITE              (0x21)
#define AR1021_CMD_REGISTER_START_ADDR_REQUEST (0x22)
#define AR1021_CMD_REGISTER_WRITE_TO_EEPROM    (0x23)
#define AR1021_CMD_EEPROM_READ                 (0x28)
#define AR1021_CMD_EEPROM_WRITE                (0x29)
#define AR1021_CMD_EEPROM_WRITE_TO_REGISTERS   (0x2B)
 
#define AR1021_RESP_STAT_OK           (0x00)
#define AR1021_RESP_STAT_CMD_UNREC    (0x01)
#define AR1021_RESP_STAT_HDR_UNREC    (0x03)
#define AR1021_RESP_STAT_TIMEOUT      (0x04)
#define AR1021_RESP_STAT_CANCEL_CALIB (0xFC)
 
 
#define AR1021_ERR_NO_HDR      (-1000)
#define AR1021_ERR_INV_LEN     (-1001)
#define AR1021_ERR_INV_RESP    (-1002)
#define AR1021_ERR_INV_RESPLEN (-1003)
#define AR1021_ERR_TIMEOUT     (-1004)


#define PEN_BYTE 0
#define XLO_BYTE 1
#define XHI_BYTE 2
#define YLO_BYTE 3
#define YHI_BYTE 4

// bit 7 is always 1 and bit 0 defines pen up or down
#define AR1021_PEN_MASK (0x81)
// bit 7 is always 1 and bit 0 defines pen up or down
#define AR1021_ONLY_PEN_MASK (1)
// bit 7 is always 1 and bit 0 defines pen up or down
#define AR1021_PCK_CHECK_MASK (0x80)


enum mgos_ar1021_touch_t {
  TOUCH_DOWN = 0,
  TOUCH_UP   = 1,
};

struct mgos_ar1021_event_data {
  enum mgos_ar1021_touch_t direction;
  uint16_t                   x;
  uint16_t                   y;
  uint16_t                   raw_x;
  uint16_t                   raw_y;
  unsigned long         length; // Amount of time the TOUCH_DOWN event lasted (always set to 1 for TOUCH_DOWN)
};

bool mgos_ar1021_i2c_init(void);

typedef void (*mgos_ar1021_event_t)(struct mgos_ar1021_event_data *);
void mgos_ar1021_set_handler(mgos_ar1021_event_t handler);
void mgos_ar1021_set_dimensions(uint16_t x, uint16_t y);
bool mgos_ar1021_init(void);
//void mgos_ar1021_set_orientation(uint8_t flags);
//bool mgos_ar1021_is_touching();

#endif // __AR1021H_

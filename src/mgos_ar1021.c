/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mgos.h"
#include "mgos_i2c.h"
#include "mgos_ar1021.h"

// #define MAX(x, y) (((x) > (y)) ? (x) : (y))
// #define MIN(x, y) (((x) < (y)) ? (x) : (y))
//#include "esp32/include/esp_timer.h"

int mgos_ar1021_cmd(char cmd, uint8_t *data, uint8_t len, uint8_t *respBuf, uint8_t *respLen);

static mgos_ar1021_event_t s_event_handler = NULL;
//static uint8_t s_orientation;// = STMPE_ORIENTATION_X | STMPE_ORIENTATION_Y | STMPE_ORIENTATION_SWAP_NONE;
//struct mgos_ar1021_event_data s_last_touch;
static uint64_t last_down_usecs;
static bool last_ev_was_up = false;
// static uint16_t s_max_x = 240;
// static uint16_t s_max_y = 320;

static long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  /*if (x < in_min)
  {
    x = in_min;
  }
  if (x > in_max)
  {
    x = in_max;
  }*/
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

bool mgos_ar1021_i2c_write_byte(struct mgos_i2c *i2c, int addr, uint8_t val)
{
  if (!mgos_i2c_write(i2c, addr, &val, 1, false /* stop */))
  {
    LOG(LL_ERROR, ("I2C write failed"));
    return false;
  };
  return true;
}

bool mgos_ar1021_i2c_read_byte(struct mgos_i2c *i2c, int addr, uint8_t *val)
{
  if (!mgos_i2c_read(i2c, addr, val, 1, false /* stop */))
  {
    LOG(LL_ERROR, ("I2C read failed"));
    return false;
  };
  return true;
}

bool mgos_ar1021_i2c_read(struct mgos_i2c *i2c, int addr, int len, uint8_t *val)
{
  if (!mgos_i2c_read(i2c, addr, val, len, true /* stop */))
  {
    LOG(LL_ERROR, ("I2C read failed"));
    return false;
  };
  return true;
}

static void ar1021_irq(int pin, void *arg)
{
  //LOG(LL_ERROR, ("irq"));
  struct mgos_ar1021_event_data ed;

  struct mgos_i2c *i2c = mgos_i2c_get_global();

  if (!i2c)
  {
    LOG(LL_ERROR, ("Cannot get global I2C bus"));
    //return 0;
  }

  uint8_t pck[6];
  bool failed = false;
  int retry = 3;
  while(retry--){
    failed = false;
    if (!mgos_ar1021_i2c_read(i2c, mgos_sys_config_get_ar1021_addr(), 5, pck))
      {
        LOG(LL_ERROR, ("I2C read failed , retry %d", retry));
        failed = true;
        //return 0;
      }
    
    // for (int i = 0; i < 5; i++)
    // {
    //   mgos_usleep(50);
    //   if (!mgos_ar1021_i2c_read_byte(i2c, i ? MGOS_I2C_ADDR_CONTINUE : mgos_sys_config_get_ar1021_addr(), pck + i))
    //   {
    //     LOG(LL_ERROR, ("I2C read failed , byte %d, retry %d", i, retry));
    //     failed = true;
    //     break;
    //     //return 0;
    //   }
      
    // }
    // mgos_i2c_stop(i2c);
    mgos_msleep(1);
    if (!failed){
      break;
    }
    
  }
  if (failed){
    return;
  }
  LOG(LL_DEBUG, ("I2C read ok"));

  // for (int i = 0; i < 5; i++)
  // {
  //   if (!mgos_ar1021_i2c_read_byte(i2c, i ? MGOS_I2C_ADDR_CONTINUE : mgos_sys_config_get_ar1021_addr(), pck + i))
  //   {
  //     LOG(LL_ERROR, ("I2C read failed"));
  //     //return 0;
  //   }
  //   mgos_usleep(50);
  // }
  // mgos_i2c_stop(i2c);

  LOG(LL_DEBUG, ("R %02X %02X %02X %02X %02X", pck[4], pck[3], pck[2], pck[1], pck[0]));

  // invalid packet
  if ((pck[PEN_BYTE] & AR1021_PCK_CHECK_MASK) != (1 << 7))
  {
    /* while (mgos_gpio_read(mgos_sys_config_get_ar1021_irq_pin()))
    {
      if (!mgos_ar1021_i2c_read_byte(i2c, mgos_sys_config_get_ar1021_addr(), pck))
      {
        if
        LOG(LL_ERROR, ("I2C read failed"));
        //return 0;
      }
      mgos_usleep(50);
    }*/
    LOG(LL_ERROR, ("invalid packet"));
    return;
  }

  int x = (pck[XHI_BYTE] << 7) | pck[XLO_BYTE], y = (pck[YHI_BYTE] << 7) | pck[YLO_BYTE];
  if (mgos_sys_config_get_ar1021_landscape()){
    int sw = x;
    x = y;
    y = sw;
  }

  ed.raw_x = x;
  ed.raw_y = y;
  if (mgos_sys_config_get_ar1021_internal_min_x() != mgos_sys_config_get_ar1021_internal_max_x())
  {
    ed.x = map(x,
               mgos_sys_config_get_ar1021_internal_min_x(),
               mgos_sys_config_get_ar1021_internal_max_x(),
               0,
               mgos_sys_config_get_ar1021_width());
    ed.x = MIN(MAX(ed.x, 0),  mgos_sys_config_get_ar1021_width());         
  }
  else
  {
    ed.x = 0;
  }
  if (mgos_sys_config_get_ar1021_internal_min_y() != mgos_sys_config_get_ar1021_internal_max_y())
  {
    ed.y = map(y,
               mgos_sys_config_get_ar1021_internal_min_y(),
               mgos_sys_config_get_ar1021_internal_max_y(),
               0,
               mgos_sys_config_get_ar1021_height());
    ed.y = MIN(MAX(ed.y, 0),  mgos_sys_config_get_ar1021_height());      
  }
  else
  {
    ed.y = 0;
  }

  // pen down
  if ((pck[PEN_BYTE] & AR1021_ONLY_PEN_MASK) == (1 << 0))
  {
    LOG(LL_DEBUG, ("pen down"));
    //_pen = 1;
    ed.direction = TOUCH_DOWN;
    if (last_ev_was_up)
    {
      last_down_usecs = esp_timer_get_time();
      ed.length = 1;
    }
    else
    {
      ed.length = (unsigned long)((esp_timer_get_time() - last_down_usecs) / 1000);
    }
    last_ev_was_up = false;
  }
  else
  { // pen up
    //_p1en = 0;
    if (!last_ev_was_up)
    {
    LOG(LL_DEBUG, ("pen up"));

      //LOG(LL_ERROR, ("you lier!!"));
      ed.direction = TOUCH_UP;
      ed.length = (unsigned long)((esp_timer_get_time() - last_down_usecs) / 1000);
      last_ev_was_up = true;
    }
    else
    {
      return;
    }
    // ed.direction = TOUCH_UP;
    // ed.length = (unsigned long)((esp_timer_get_time() - last_down_usecs) / 1000);
    // last_ev_was_up = true;
  }

  if (s_event_handler)
  {
    s_event_handler(&ed);
  }

  (void)pin;
  (void)arg;
}

void mgos_ar1021_set_handler(mgos_ar1021_event_t handler)
{
  s_event_handler = handler;
}

// void mgos_ar1021_set_dimensions(uint16_t x, uint16_t y)
// {
//   s_max_x = x;
//   s_max_y = y;
// }

bool mgos_ar1021_i2c_init(void)
{
  mgos_gpio_set_mode(mgos_sys_config_get_ar1021_irq_pin(), MGOS_GPIO_MODE_INPUT);
  mgos_gpio_set_pull(mgos_sys_config_get_ar1021_irq_pin(), MGOS_GPIO_PULL_DOWN);

  struct mgos_i2c *i2c = mgos_i2c_get_global();

  if (!i2c)
  {
    LOG(LL_ERROR, ("Cannot get global I2C bus"));
    //return 0;
  }

  mgos_msleep(1);
  uint8_t pck[6];

  LOG(LL_ERROR, ("IRQ: %d", mgos_gpio_read(mgos_sys_config_get_ar1021_irq_pin())));
  while (mgos_gpio_read(mgos_sys_config_get_ar1021_irq_pin()))
  {
    for (int i = 0; i < 5; i++)
    {
      if (!mgos_ar1021_i2c_read_byte(i2c, i ? MGOS_I2C_ADDR_CONTINUE : mgos_sys_config_get_ar1021_addr(), pck + i))
      {
        //LOG(LL_ERROR, ("I2C read failed"));
        //return 0;
      }
      mgos_usleep(50);
    }
    mgos_i2c_stop(i2c);
    LOG(LL_ERROR, ("ar1021 read ok"));
    //LOG(LL_INFO, ("R %02X %02X %02X %02X %02X", pck[4], pck[3], pck[2], pck[1], pck[0]));
    //mgos_msleep(1);
    //LOG(LL_ERROR, ("IRQ: %d", mgos_gpio_read(mgos_sys_config_get_ar1021_irq_pin())));
  }

  mgos_gpio_set_int_handler(mgos_sys_config_get_ar1021_irq_pin(), MGOS_GPIO_INT_EDGE_POS, ar1021_irq, NULL);
  mgos_gpio_enable_int(mgos_sys_config_get_ar1021_irq_pin());

  LOG(LL_INFO, ("AR1021 init (ADDR 0x%X, IRQ: %d)", mgos_sys_config_get_ar1021_addr(), mgos_sys_config_get_ar1021_irq_pin()));
  LOG(LL_INFO, ("AR1021 mapping X(%d - %d => 0 - %d) Y(%d - %d => 0 - %d)",
                mgos_sys_config_get_ar1021_internal_min_x(),
                mgos_sys_config_get_ar1021_internal_max_x(),
                mgos_sys_config_get_ar1021_width(),
                mgos_sys_config_get_ar1021_internal_min_y(),
                mgos_sys_config_get_ar1021_internal_max_y(),
                mgos_sys_config_get_ar1021_height()));

  return true;
}

// bool mgos_ar1021_init(void) {
//   mgos_gpio_set_mode(mgos_sys_config_get_ar1021_irq_pin(), MGOS_GPIO_MODE_INPUT);
//   //mgos_gpio_set_pull(mgos_sys_config_get_ar1021_irq_pin(), MGOS_GPIO_PULL_UP);
//   mgos_gpio_set_int_handler(mgos_sys_config_get_ar1021_irq_pin(), MGOS_GPIO_INT_EDGE_POS, ar1021_irq, NULL);
//   mgos_gpio_enable_int(mgos_sys_config_get_ar1021_irq_pin());

//     int result = 0;
//     bool ok = false;
//     int attempts = 0;

//  /*    _width = width;
//     _height = height;
//  */
//     while (1) {
//         do {
//             // disable touch
//             result = mgos_ar1021_cmd(AR1021_CMD_DISABLE_TOUCH, NULL, 0, NULL, 0);
//             LOG(LL_DEBUG, ("res %02X, exp1 %02X, exp2 %02X", (uint8_t)result, (uint8_t)AR1021_RESP_STAT_CANCEL_CALIB, (uint8_t)-AR1021_RESP_STAT_CANCEL_CALIB));
//             if ((uint8_t)result == (uint8_t)AR1021_RESP_STAT_CANCEL_CALIB) {
//                 LOG(LL_DEBUG, ("calibration was cancelled, short delay and try again"));
//                 mgos_usleep(50);
//                 result = mgos_ar1021_cmd(AR1021_CMD_DISABLE_TOUCH, NULL, 0, NULL, 0);
//                 LOG(LL_DEBUG, ("res2 %02X", (uint8_t)result));
//             }
//             if (result != 0) {
//                 LOG(LL_DEBUG, ("disable touch failed (%d)\n", result));
//                 break;
//             }
//             mgos_usleep(50);

//             uint8_t regOffset = 0;
//             uint8_t regOffLen = 1;
//             result = mgos_ar1021_cmd(AR1021_CMD_REGISTER_START_ADDR_REQUEST, NULL, 0,
//                     &regOffset, &regOffLen);
//             if (result != 0) {
//                 LOG(LL_DEBUG, ("register offset request failed (%d)\n", result));
//                 break;
//             }

//             // enable calibrated coordinates
//             //                  high, low address,                        len,  value
//             uint8_t toptions[4] = {0x00, AR1021_REG_TOUCH_OPTIONS+regOffset, 0x01, 0x01};
//             result = mgos_ar1021_cmd(AR1021_CMD_REGISTER_WRITE, toptions, 4, NULL, 0);
//             if (result != 0) {
//                 LOG(LL_DEBUG, ("register write request failed (%d)\n", result));
//                 break;
//             }

//             // save registers to eeprom
//             result = mgos_ar1021_cmd(AR1021_CMD_REGISTER_WRITE_TO_EEPROM, NULL, 0, NULL, 0);
//             if (result != 0) {
//                 LOG(LL_DEBUG, ("register write to eeprom failed (%d)\n", result));
//                 break;
//             }

//             // enable touch
//             result = mgos_ar1021_cmd(AR1021_CMD_ENABLE_TOUCH, NULL, 0, NULL, 0);
//             if (result != 0) {
//                 LOG(LL_DEBUG, ("enable touch failed (%d)\n", result));
//                 break;
//             }

//             //_siqIrq.rise(this, &AR1021::readTouchIrq);

//             LOG(LL_INFO, ("AR1021 init (ADDR%d, IRQ: %d)", mgos_sys_config_get_ar1021_addr(), mgos_sys_config_get_ar1021_irq_pin()));
//             //_initialized = true;
//             ok = true;

//         } while(0);

//         if (ok) break;

//         // try to run the initialize sequence at most 2 times
//         if(++attempts >= 5) break;
//     }

//     return ok;
// }

// int mgos_ar1021_cmd(char cmd, uint8_t* data, uint8_t len, uint8_t* respBuf, uint8_t* respLen) {
//   struct mgos_i2c *i2c = mgos_i2c_get_global();

//   if (!i2c) {
//     LOG(LL_ERROR, ("Cannot get global I2C bus"));
//     //return 0;
//   }

//   int ret = 0;

//   // command request
//   // ---------------
//   // 0x55 len cmd data
//   // 0x55 = header
//   // len = data length + cmd (1)
//   // data = data to send
//   mgos_ar1021_i2c_write_byte(i2c, mgos_sys_config_get_ar1021_addr(), 0x00);
//   mgos_usleep(50); // according to data sheet there must be an inter-byte delay of ~50us
//   mgos_ar1021_i2c_write_byte(i2c, MGOS_I2C_ADDR_CONTINUE, 0x55);
//   mgos_usleep(50); // according to data sheet there must be an inter-byte delay of ~50us
//   mgos_ar1021_i2c_write_byte(i2c, MGOS_I2C_ADDR_CONTINUE, len+1);
//   mgos_usleep(50);
//   mgos_ar1021_i2c_write_byte(i2c, MGOS_I2C_ADDR_CONTINUE, cmd);
//   mgos_usleep(50);

//   for(int i = 0; i < len; i++) {
//       mgos_ar1021_i2c_write_byte(i2c, MGOS_I2C_ADDR_CONTINUE, data[i]);
//       mgos_usleep(50);
//   }
//   mgos_i2c_stop(i2c);

//   // wait for response (siq goes high when response is available)
//   uint64_t start_ts = esp_timer_get_time();
//   mgos_msleep(90);
//   //while(mgos_gpio_read(mgos_sys_config_get_ar1021_irq_pin()) != 1 && (esp_timer_get_time() - start_ts) < 500000L);
//   LOG(LL_DEBUG, ("ts %lu", (long unsigned int)(esp_timer_get_time() - start_ts)/1000));

//   // command response
//   // ---------------
//   // 0x55 len status cmd data
//   // 0x55 = header
//   // len = number of bytes following the len byte
//   // status = status
//   // cmd = command ID
//   // data = data to receive

//   do {

//       if ((esp_timer_get_time() - start_ts) >= 600000L) {
//           ret = AR1021_ERR_TIMEOUT;
//           break;
//       }

//       uint8_t head;
//       mgos_ar1021_i2c_read_byte(i2c, mgos_sys_config_get_ar1021_addr(), &head);
//       LOG(LL_DEBUG, ("res cmd %02X", head));
//       if (head != 0x55) {
//           ret = AR1021_ERR_NO_HDR;
//           break;
//       }

//       mgos_usleep(50);
//       mgos_ar1021_i2c_read_byte(i2c, MGOS_I2C_ADDR_CONTINUE, &len);
//       if (len < 2) {
//           ret = AR1021_ERR_INV_LEN;
//           break;
//       }
// LOG(LL_DEBUG, ("res cmd %02X, %02X", head, len));
//       mgos_usleep(50);
//       uint8_t status;
//       mgos_ar1021_i2c_read_byte(i2c, MGOS_I2C_ADDR_CONTINUE, &status);
//       LOG(LL_DEBUG, ("res cmd %02X, %02X, %02X", head, len, status));
//       if (status != AR1021_RESP_STAT_OK) {
//           ret = -status;
//           break;
//       }

//       mgos_usleep(50);
//       uint8_t cmdId;
//       mgos_ar1021_i2c_read_byte(i2c, MGOS_I2C_ADDR_CONTINUE, &cmdId);
//       LOG(LL_DEBUG, ("res cmd %02X, %02X, %02X, %02X", head, len, status, cmdId));
//       if (cmdId != cmd) {
//           ret = AR1021_ERR_INV_RESP;
//           break;
//       }
//       if ( ((len-2) > 0 && respLen  == NULL)
//               || ((len-2) > 0 && respLen != NULL && *respLen < (len-2))) {
//           ret = AR1021_ERR_INV_RESPLEN;
//           break;
//       }

//       for (int i = 0; i < len-2;i++) {
//           mgos_usleep(50);
//           mgos_ar1021_i2c_read_byte(i2c, MGOS_I2C_ADDR_CONTINUE, respBuf + i);
//           //respBuf[i] = _spi.write(0);
//       }
//       if (respLen != NULL) {
//           *respLen = len-2;
//       }

//       // make sure we wait 50us before issuing a new cmd
//       LOG(LL_DEBUG, ("res cmd %02X, %02X, %02X, %02X, %02X", head, len, status, cmdId, *respLen));

//   } while (0);
//   mgos_usleep(50);
//   mgos_i2c_stop(i2c);

//   return ret;
// }

// static uint8_t stmpe610_i2c_read_register(uint8_t reg) {
//   struct mgos_i2c *i2c = mgos_i2c_get_global();

//   if (!i2c) {
//     LOG(LL_ERROR, ("Cannot get global I2C bus"));
//     return 0;
//   }

//   uint8_t buf;

//   if(!mgos_i2c_write(i2c, mgos_sys_config_get_stmpe610_addr(), &reg, 1, true /* stop */)){
//     LOG(LL_ERROR, ("I2C read failed"));
//     return 0;
//   }else if(!mgos_i2c_read(i2c, mgos_sys_config_get_stmpe610_addr(), &buf, 1, true /* stop */)){
//     LOG(LL_ERROR, ("I2C read failed"));
//     return 0;
//   }

//   return buf;
// }

// static void stmpe610_i2c_write_register(uint8_t reg, uint8_t val) {
//   struct mgos_i2c *i2c = mgos_i2c_get_global();

//   if (!i2c) {
//     LOG(LL_ERROR, ("Cannot get global I2C bus"));
//     return;
//   }

//   //uint8_t tx_data[2] = { reg, val };

//   if(!mgos_i2c_write(i2c, mgos_sys_config_get_stmpe610_addr(), &reg, 1, false /* stop */)){
//     LOG(LL_ERROR, ("I2C write failed"));
//     mgos_i2c_stop(i2c);
//     return;
//   }else if(!mgos_i2c_write(i2c, MGOS_I2C_ADDR_CONTINUE, &val, 1, true /* stop */)){
//     LOG(LL_ERROR, ("I2C write failed"));
//     return;
//   }
// }

// static uint8_t stmpe610_get_bufferlength(void) {
//   return stmpe610_i2c_read_register(STMPE_FIFO_SIZE);
// }

// static uint8_t stmpe610_read_data(uint16_t *x, uint16_t *y, uint8_t *z) {
//   uint8_t  data[4];
//   uint8_t  samples, cnt;
//   uint32_t sum_sample_x = 0, sum_sample_y = 0;
//   uint16_t sum_sample_z = 0;

//   samples = cnt = stmpe610_get_bufferlength();
//   LOG(LL_DEBUG, ("Touch sensed with %d samples", samples));
//   if (samples == 0) {
//     return 0;
//   }

//   while (cnt > 0) {
//     uint16_t sample_coord1, sample_coord2;
//     uint8_t  sample_z;
//     for (uint8_t i = 0; i < 4; i++) {
//       data[i] = stmpe610_i2c_read_register(0xD7);
//     }
//     sample_coord1   = data[0];
//     sample_coord1 <<= 4;
//     sample_coord1  |= (data[1] >> 4);
//     if (s_orientation & STMPE_ORIENTATION_FLIP_X) {
//       sample_coord1 = 4096 - sample_coord1;
//     }
//     sample_coord2   = data[1] & 0x0F;
//     sample_coord2 <<= 8;
//     sample_coord2  |= data[2];
//     if (s_orientation & STMPE_ORIENTATION_FLIP_Y) {
//       sample_coord2 = 4096 - sample_coord2;
//     }
//     if (s_orientation & STMPE_ORIENTATION_SWAP_XY) {
//       uint32_t sample_swap = sample_coord1;
//       sample_coord1 = sample_coord2;
//       sample_coord2 = sample_swap;
//     }
//     sample_z      = data[3];
//     sum_sample_x += sample_coord1;
//     sum_sample_y += sample_coord2;
//     sum_sample_z += sample_z;
//     LOG(LL_DEBUG, ("Sample at (%d,%d) pressure=%d, bufferLength=%d", sample_coord1, sample_coord2, sample_z, stmpe610_get_bufferlength()));
//     cnt--;
//   }
//   *x = map(sum_sample_x / samples, 150, 3800, 0, s_max_x);
//   *y = map(sum_sample_y / samples, 150, 3800, 0, s_max_y);
//   *z = sum_sample_z / samples;

//   stmpe610_i2c_write_register(STMPE_FIFO_STA, STMPE_FIFO_STA_RESET); // clear FIFO
//   stmpe610_i2c_write_register(STMPE_FIFO_STA, 0);                    // unreset

//   return samples;
// }

// static uint16_t smpe610_get_version() {
//   uint16_t version;

//   version   = stmpe610_i2c_read_register(0);
//   version <<= 8;
//   version  |= stmpe610_i2c_read_register(1);

//   LOG(LL_INFO, ("Read Version byte: 0x%04x", version));
//   return version;
// }

// /* Each time a TOUCH_DOWN event occurs, s_last_touch is populated
//  * and a timer is started. When the timer fires, we checke to see
//  * if we've sent a TOUCH_UP event already. If not, we may still be
//  * pressing the screen. IF we're not pressing the screen STMP610
//  * bufferLength() will be 0. We've now detected a DOWN without a
//  * corresponding UP, so we send it ourselves.
//  */
// static void stmpe610_down_cb(void *arg) {
//   if (s_last_touch.direction == TOUCH_UP) {
//     return;
//   }
//   if (stmpe610_get_bufferlength() > 0) {
//     return;
//   }

//   s_last_touch.direction = TOUCH_UP;
//   if (s_event_handler) {
//     LOG(LL_INFO, ("Touch DOWN not followed by UP -- sending phantom UP"));
//     if (s_event_handler) {
//       s_event_handler(&s_last_touch);
//     }
//   }
//   (void)arg;
// }

// static void stmpe610_irq(int pin, void *arg) {
//   struct mgos_stmpe610_event_data ed;

//   if (stmpe610_get_bufferlength() == 0) {
//     uint8_t i;
//     LOG(LL_DEBUG, ("Touch DOWN"));
//     for (i = 0; i < 10; i++) {
//       mgos_msleep(5);
//       if (stmpe610_get_bufferlength() > 0) {
//         stmpe610_read_data(&ed.x, &ed.y, &ed.z);
//         LOG(LL_DEBUG, ("Touch DOWN at (%d,%d) pressure=%d, length=%d, iteration=%d", ed.x, ed.y, ed.z, ed.length, i));

//         ed.length    = 1;
//         ed.direction = TOUCH_DOWN;
//         // To avoid DOWN events without an UP event, set a timer (see stmpe610_down_cb for details)
//         memcpy((void *)&s_last_touch, (void *)&ed, sizeof(s_last_touch));
//         mgos_set_timer(100, 0, stmpe610_down_cb, NULL);
//         if (s_event_handler) {
//           s_event_handler(&ed);
//         }
//         break;
//       }
//     }
//     stmpe610_i2c_write_register(STMPE_INT_STA, 0xFF); // reset all ints
//     return;
//   }

//   ed.length = stmpe610_read_data(&ed.x, &ed.y, &ed.z);
//   LOG(LL_DEBUG, ("Touch UP at (%d,%d) pressure=%d, length=%d", ed.x, ed.y, ed.z, ed.length));
//   ed.direction = TOUCH_UP;
//   memcpy((void *)&s_last_touch, (void *)&ed, sizeof(s_last_touch));
//   if (s_event_handler) {
//     s_event_handler(&ed);
//   }

//   stmpe610_i2c_write_register(STMPE_INT_STA, 0xFF); // reset all ints
//   (void)pin;
//   (void)arg;
// }

// void mgos_stmpe610_set_handler(mgos_stmpe610_event_t handler) {
//   s_event_handler = handler;
// }

// void mgos_stmpe610_set_orientation(uint8_t flags) {
//   s_orientation = flags;
// }

// bool mgos_stmpe610_is_touching() {
//   return s_last_touch.direction == TOUCH_DOWN;
// }

// void mgos_stmpe610_set_dimensions(uint16_t x, uint16_t y) {
//   s_max_x = x;
//   s_max_y = y;
// }

// bool mgos_stmpe610_i2c_init(void) {
//   uint16_t v;

//   mgos_gpio_set_mode(mgos_sys_config_get_stmpe610_irq_pin(), MGOS_GPIO_MODE_INPUT);
//   mgos_gpio_set_pull(mgos_sys_config_get_stmpe610_irq_pin(), MGOS_GPIO_PULL_UP);
//   mgos_gpio_set_int_handler(mgos_sys_config_get_stmpe610_irq_pin(), MGOS_GPIO_INT_EDGE_NEG, stmpe610_irq, NULL);
//   mgos_gpio_enable_int(mgos_sys_config_get_stmpe610_irq_pin());

//   stmpe610_i2c_write_register(STMPE_SYS_CTRL1, STMPE_SYS_CTRL1_RESET);
//   mgos_msleep(10);

//   v = smpe610_get_version();
//   LOG(LL_INFO, ("STMPE610 init (ADDR%d, IRQ: %d)", mgos_sys_config_get_stmpe610_addr(), mgos_sys_config_get_stmpe610_irq_pin()));
//   if (0x811 != v) {
//     LOG(LL_ERROR, ("STMPE610 init failed (0x%04x), disabling", v));
//     return true;
//   }
//   LOG(LL_INFO, ("STMPE610 init ok"));

//   for (uint8_t i = 0; i < 65; i++) {
//     stmpe610_i2c_read_register(i);
//   }

//   stmpe610_i2c_write_register(STMPE_SYS_CTRL2, 0x0);                                   // turn on clocks!
//   stmpe610_i2c_write_register(STMPE_TSC_CTRL, STMPE_TSC_CTRL_XYZ | STMPE_TSC_CTRL_EN); // XYZ and enable!
//   stmpe610_i2c_write_register(STMPE_INT_EN, STMPE_INT_EN_TOUCHDET);
//   stmpe610_i2c_write_register(STMPE_ADC_CTRL1, STMPE_ADC_CTRL1_10BIT | (0x6 << 4));    // 96 clocks per conversion
//   stmpe610_i2c_write_register(STMPE_ADC_CTRL2, STMPE_ADC_CTRL2_6_5MHZ);
//   stmpe610_i2c_write_register(STMPE_TSC_CFG, STMPE_TSC_CFG_4SAMPLE | STMPE_TSC_CFG_DELAY_1MS | STMPE_TSC_CFG_SETTLE_5MS);
//   stmpe610_i2c_write_register(STMPE_TSC_FRACTION_Z, 0x6);
//   stmpe610_i2c_write_register(STMPE_FIFO_TH, 1);
//   stmpe610_i2c_write_register(STMPE_FIFO_STA, STMPE_FIFO_STA_RESET);
//   stmpe610_i2c_write_register(STMPE_FIFO_STA, 0);   // unreset
//   stmpe610_i2c_write_register(STMPE_TSC_I_DRIVE, STMPE_TSC_I_DRIVE_50MA);
//   stmpe610_i2c_write_register(STMPE_INT_STA, 0xFF); // reset all ints
//   stmpe610_i2c_write_register(STMPE_INT_CTRL, STMPE_INT_CTRL_POL_LOW | STMPE_INT_CTRL_EDGE | STMPE_INT_CTRL_ENABLE);

//   // Initialize the last touch to TOUCH_UP
//   s_last_touch.direction = TOUCH_UP;
//   s_last_touch.x         = 0;
//   s_last_touch.y         = 0;
//   s_last_touch.z         = 0;
//   s_last_touch.length    = 0;

//   // Set the orientation
//   s_orientation = mgos_sys_config_get_stmpe610_orientation();

//   return true;
// }
